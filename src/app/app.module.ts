import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { AngularFireModule } from 'angularfire2'; //Importar AngularFireModule
import { AngularFireAuthModule, AngularFireAuth } from 'angularfire2/auth'; //Importar AngularFireAuthModule


import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { SigninPage } from '../pages/signin/signin'; //Importar pagina de Login
import { SignupPage } from '../pages/signup/signup'; //Importar pagina de Cadastro
import { ResetpasswordPage } from '../pages/resetpassword/resetpassword'; //Importar pagina de Resetar senha

import { AuthService } from '../providers/auth/auth-service'; //Importar AuthService e colocar ele em Providers
// Paginas
import { DestaquesPage } from '../pages/destaques/destaques';
import { MapsPage } from '../pages//maps/maps';
import { ContasPage } from '../pages/contas/contas';
import { LiderancaPage } from '../pages/lideranca/lideranca';
import { SobrePage } from '../pages/sobre/sobre';
import { EnderecoPage } from '../pages/endereco/endereco';
import { HorariosPage } from '../pages/horarios/horarios';
import { RedesocialPage } from '../pages/redesocial/redesocial';
import { PedidosPage } from '../pages/pedidos/pedidos';

const firebaseConfig = { //Colocar configurações do fire base aqui!
    apiKey: "AIzaSyByMtXabtpgQkNk7dZ51rgp1GLtpI_pviE",
    authDomain: "iapda-cbe3a.firebaseapp.com",
    databaseURL: "https://iapda-cbe3a.firebaseio.com",
    projectId: "iapda-cbe3a",
    storageBucket: "iapda-cbe3a.appspot.com",
    messagingSenderId: "1029536722967"
};

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    SigninPage,
    SignupPage,
    ResetpasswordPage,
    DestaquesPage,
    LiderancaPage,
    MapsPage,
    ContasPage,
    SobrePage,
    EnderecoPage,
    HorariosPage,
    RedesocialPage,
    PedidosPage,

    
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    AngularFireModule.initializeApp(firebaseConfig), // <<<< Depois de criar a const firebaseConfig declarar ela aqui <<<<<
    AngularFireAuthModule // Importar AngularFireModule aqui <<<
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    SigninPage, // Login
    SignupPage, // Cadastro
    ResetpasswordPage, //Esqueceu a senha
    DestaquesPage,
    LiderancaPage,
    MapsPage,
    ContasPage,
    SobrePage,
    EnderecoPage,
    HorariosPage,
    RedesocialPage,
    PedidosPage,


  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    AuthService, // AuthService colocar ele aqui!!
  ]
})
export class AppModule {}
