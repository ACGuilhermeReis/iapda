import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HomePage } from '../pages/home/home';
import { SigninPage } from '../pages/signin/signin'; //Importar pagina de Login
import { SignupPage } from '../pages/signup/signup'; //Importar pagina de Cadastro
import { ResetpasswordPage } from '../pages/resetpassword/resetpassword'; //Importar pagina de Resetar senha
import { AngularFireAuth } from 'angularfire2/auth';

//
import { DestaquesPage } from '../pages/destaques/destaques';


@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage: any;

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen, afAuth: AngularFireAuth) {
    const authObservable = afAuth.authState.subscribe(user => {
      if (user){
        this.rootPage = HomePage;
       
      } else{
        this.rootPage = SigninPage;
        
      }
    })
    platform.ready().then(() => {

      statusBar.styleDefault();
      splashScreen.hide();
    });
  }
}

