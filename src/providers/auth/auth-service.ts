import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { AngularFireAuth } from 'angularfire2/auth';
import { User } from './user';
import * as firebase from 'firebase/app';
 

@Injectable()
export class AuthService {
  user: Observable<firebase.User>;

  constructor(private angularFireAuth: AngularFireAuth) {
    this.user = angularFireAuth.authState;
   }

  // Criar Usuário
  createUser(user: User) {
    return this.angularFireAuth.auth.createUserWithEmailAndPassword(user.email, user.password);
  } // Criar Usuário


  
  signIn(user: User) { //Criar metodo Entrar
    return this.angularFireAuth.auth.signInWithEmailAndPassword(user.email, user.password);
  }//Criar metodo Entrar
  
  signOut(){ //Criar metodo Sair
    return this.angularFireAuth.auth.signOut();
  }//Criar metodo Sair

  



  resetPassword(email: string) {
    return this.angularFireAuth.auth.sendPasswordResetEmail(email);
  }
}
