import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SignupPage } from './signup';

import { AngularFireModule } from 'angularfire2'; //Importar AngularFireModule
import { AngularFireAuthModule } from 'angularfire2/auth'; //Importar AngularFireAuthModule
import { AuthService } from '../../providers/auth/auth-service'; //Importar AuthService e colocar ele em Providers

@NgModule({
  declarations: [
    SignupPage,
  ],
  imports: [
    IonicPageModule.forChild(SignupPage),
  ],
})
export class SignupPageModule {}
