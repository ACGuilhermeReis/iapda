import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RedesocialPage } from './redesocial';

@NgModule({
  declarations: [
    RedesocialPage,
  ],
  imports: [
    IonicPageModule.forChild(RedesocialPage),
  ],
})
export class RedesocialPageModule {}
