import { Component } from '@angular/core';
import { NavController, NavParams} from 'ionic-angular';
import { User } from '../../providers/auth/user';
import { AuthService } from '../../providers/auth/auth-service';
import { SigninPage } from '../signin/signin'; //Importar pagina de Login
import { SignupPage } from '../signup/signup';

//Pages Menu
import { DestaquesPage } from '../destaques/destaques';
import { LiderancaPage } from '../lideranca/lideranca';
import { MapsPage } from '../maps/maps';
import { ContasPage } from '../contas/contas';
import { SobrePage } from '../sobre/sobre';
import { EnderecoPage } from '../endereco/endereco';
import { HorariosPage } from '../horarios/horarios';
import { RedesocialPage } from '../redesocial/redesocial'; 
import { PedidosPage } from '../pedidos/pedidos';


@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(public navCtrl: NavController, private authService: AuthService) {

  }
  signOut(){
    this.authService.signOut()
    .then(() => {
      this.navCtrl.setRoot(SigninPage);
    })
    
    .catch((error) => {
      console.error(error);
    });
  }
  //Routers Pages Menu
  destaquesPage(){
    this.navCtrl.push(DestaquesPage);
  }
  liderancaPage(){
    this.navCtrl.push(LiderancaPage);
  }
  mapaPage(){
    this.navCtrl.push(MapsPage);
  }
  contaPage(){
    this.navCtrl.push(ContasPage);
  }
  sobrePage(){
    this.navCtrl.push(SobrePage);
  }
  enderecoPage(){
    this.navCtrl.push(EnderecoPage);
  }
  hoariosPage(){
    this.navCtrl.push(HorariosPage);
  }
  social(){
    this.navCtrl.push(RedesocialPage);
  }
  pedidosPage(){
    this.navCtrl.push(PedidosPage);
  }







}
