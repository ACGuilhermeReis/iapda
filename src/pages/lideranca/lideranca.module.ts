import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LiderancaPage } from './lideranca';

@NgModule({
  declarations: [
    LiderancaPage,
  ],
  imports: [
    IonicPageModule.forChild(LiderancaPage),
  ],
})
export class LiderancaPageModule {}
